<?php
return [
    '/oauth/employee/create' => [
        'type' => 2,
        'description' => 'Create a Emp',
    ],
    '/oauth/employee/update' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    '/oauth/employee/delete' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    '/oauth/employee/index' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    '/oauth/employee/view' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    'oauth/employee/create' => [
        'type' => 2,
        'description' => 'Create a Emp',
    ],
    'oauth/employee/update' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    'oauth/employee/delete' => [
        'type' => 2,
        'description' => 'delete Emp',
    ],
    'oauth/employee/index' => [
        'type' => 2,
        'description' => 'index Emp',
    ],
    'oauth/employee/view' => [
        'type' => 2,
        'description' => 'view Emp',
    ],
    '/employee/create' => [
        'type' => 2,
        'description' => 'Create a Emp',
    ],
    '/employee/update' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    '/employee/delete' => [
        'type' => 2,
        'description' => 'delete Emp',
    ],
    '/employee/index' => [
        'type' => 2,
        'description' => 'index Emp',
    ],
    '/employee/view' => [
        'type' => 2,
        'description' => 'view Emp',
    ],
    'createEmp' => [
        'type' => 2,
        'description' => 'Create a Emp',
    ],
    'updateEmp' => [
        'type' => 2,
        'description' => 'Update Emp',
    ],
    'deleteEmp' => [
        'type' => 2,
        'description' => 'delete Emp',
    ],
    'indexEmp' => [
        'type' => 2,
        'description' => 'index Emp',
    ],
    'viewEmp' => [
        'type' => 2,
        'description' => 'view Emp',
    ],
    'author' => [
        'type' => 1,
        'children' => [
            'oauth/employee/create',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'oauth/employee/update',
            'author',
        ],
    ],
];
