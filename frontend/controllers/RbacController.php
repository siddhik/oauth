<?php

namespace frontend\controllers;

use Yii;
use common\modules\auth\models\AuthItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RbacController implements the CRUD actions for AuthItem model.
 */
class RbacController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    
    public function actionCreate_role(){
           $auth = Yii::$app->authManager;
        // add "createPost" permission
        $createEmp = $auth->createPermission('oauth/employee/create');

        // add "updatePost" permission
        $updateEmp = $auth->createPermission('oauth/employee/update');

          $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createEmp);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updateEmp);
        $auth->addChild($admin, $author);
    }
    public function actionCreate_permission() {

        $auth = Yii::$app->authManager;
        // add "createPost" permission
        $createEmp = $auth->createPermission('oauth/employee/create');
        $createEmp->description = 'Create a Emp';
        $auth->add($createEmp);

        // add "updatePost" permission
        $updateEmp = $auth->createPermission('oauth/employee/update');
        $updateEmp->description = 'Update Emp';
        $auth->add($updateEmp);

        $deleteEmp = $auth->createPermission('oauth/employee/delete');
        $deleteEmp->description = 'delete Emp';
        $auth->add($deleteEmp);

        $indexEmp = $auth->createPermission('oauth/employee/index');
        $indexEmp->description = 'index Emp';
        $auth->add($indexEmp);
        
        $viewEmp = $auth->createPermission('oauth/employee/view');
        $viewEmp->description = 'view Emp';
        $auth->add($viewEmp);

    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => AuthItem::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
