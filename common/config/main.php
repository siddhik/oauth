<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
//         'response' => [
//        'format' => 'json'
//    ]
    ],
    'modules' => [
        'auth' => [
            'class' => 'common\modules\auth\Module',
        ],
    ],
   
];
